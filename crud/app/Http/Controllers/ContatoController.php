<?php

namespace App\Http\Controllers;

use App\Contato;
use Illuminate\Http\Request;

class ContatoController extends Controller
{
    public function index()
    {
        $contatos = Contato::all();
        //mesma coisa que SELECT * FROM contatos
        return view('contatos.main', ['contatos' => $contatos]);
    }

    public function add()
    {
        return view('contatos.add');
    }

    public function show($id)
    {
        $contato = Contato::find($id);
        return view('contatos.show', ['contato' => $contato]);
    }

    public function edit($id)
    {   
        $contato = Contato::find($id);
        return view('contatos.edit', ['contato' => $contato]);
    }

    public function store(Request $request)
    {
        Contato::create($request->all());
        return 'Cadastrado com sucesso';
    }

    public function update(Request $request, $id)
    {  
        // pegando um elemento pelo post e nao todos
        // $data = $request->all();
        // $nome = $data['nome'];

        //esse pega todos os posts
        $contato = Contato::find($id);
        $contato->update($request->all());

        return "<h1>Dados alterados com sucesso</h1>";

    }

    public function delete($id)
    {
        Contato::find($id)->delete();
        return redirect('/');
    }

}
