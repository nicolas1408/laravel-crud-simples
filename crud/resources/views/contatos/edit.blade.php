@extends('base.main') @section('content')
<h1>Editar contatos</h1>

<form action="{{route('contato.post.deletar', $contato->id)}}" method="post">
    <input type="hidden" name="_method" value="DELETE"> {{csrf_field()}}
    <input type="submit" value="deletar">
</form>

<form action="{{route('contato.post.alterar', $contato->id)}}" method="post">
    <input type="hidden" name="_method" value="PUT"> {{csrf_field()}}
    <p>
        <label for="nome">Nome</label>
        <input type="text" name="nome" id="" value="{{$contato->nome}}">
    </p>
    <p>
        <label for="email">Email</label>
        <input type="text" name="email" id="" value="{{$contato->email}}">
    </p>
    <p>
        <label for="telefone">Telefone</label>
        <input type="text" name="telefone" id="" value="{{$contato->telefone}}">
    </p>
    <p>
        <input type="submit" value="salvar">
    </p>
</form>
@endsection