<?php

//rotas de views
Route::get('/', 'HomeController@showLoginForm');

//controller
Route::get('/adicionar', ['as' => 'contato.adicionar', 'uses' => 'ContatoController@add']);
Route::get('/exibir/{id}', ['as' => 'contato.exibir', 'uses' => 'ContatoController@show']);
Route::get('/edit/{id}', ['as' => 'contato.editar', 'uses' => 'ContatoController@edit']);

// rotas de intercao com  banco
Route::post('/adicionar', ['as' => 'contato.post.adicionar', 'uses' => 'ContatoController@store']);
Route::put('/atualizar/{id}', ['as' => 'contato.post.alterar', 'uses' => 'ContatoController@update']);
Route::delete('/deletar/{id}', ['as' => 'contato.post.deletar', 'uses' => 'ContatoController@delete']);


//Quando tem ? e pq o parametro e opcional, quando n tem e pq e obrigatorio
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
